<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title><?php echo $title; ?></title>
    <base href="<?php echo $base; ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type="text/javascript" src="<?php echo $application; ?>vendor/jquery/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="<?php echo $application; ?>vendor/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo $application; ?>vendor/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="view/stylesheet/stylesheet.css" />
  </head>
  <body>
    <div class="container">
      <div class="row" style="text-align:center;font-weight:600;font-size:20px;padding: 5px 0px;">
        GST TAX INVOICE
      </div>
      <div class="row" style="border: 2px solid black;">
        <div class="col-xs-6" style="border-right: 1px solid black;">
          <h3 style="text-transform: uppercase;font-weight: bold;"><?php echo $system_company; ?></h3>
          <p><?php echo $system_address; ?></p>
          <p>Contact : <?php echo $system_telephone; ?> | <?php echo $system_email; ?></p>
          <h5><b>GSTIN : 20BJEPA5588B1ZS </b></h5>
        </div>
        <div class="col-xs-6" style="padding: 0px 0px;">
          <table class="table table-bordered">
            <tr>
              <td>Invoice No. : <b><?php echo $invoice_prefix; ?><?php echo $invoice_id; ?></b></td>
              <td>State : Jharkhand</td>
            </tr>
            <tr>
              <td>Date of Issue : <b><?php echo $date_issued; ?></b></td>
              <td>State Code : 20</td>
            </tr>
            <tr>
              <td>Delivery Note : </td>
              <td>Destination : <b>RANCHI</b></td>
            </tr>
          </table>
        </div>
      </div>
      <!-- <div class="row" style="border: 2px solid black;">
        <div class="col-xs-12">
          <h3 class="status"><center><b>Bill Of Supply</b></center></h3><br>
          <table class="table table-bordered">
            <tr>
              <td>Invoice No. : <?php echo $invoice_prefix; ?><?php echo $invoice_id; ?></td>
              <td>State : Jharkhand</td>
            </tr>
            <tr>
              <td>Date of Issue : <?php echo $date_issued; ?></td>
              <td>State Code : 20</td>
            </tr>
        </table>
        </div>
      </div> -->
      <div class="row" style="border: 2px solid black;">
        <div class="col-xs-12" style="padding: 0px 0px;">
          <table class="table" style="margin-bottom: 5px;">
            <thead>
              <tr>
                <th>Customer Detail</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><b>Name : </b><?php echo $firstname; ?> <?php echo $lastname; ?></td>
              </tr>
              <tr>
                <td><b>Address :</b>
                  <?php if ($payment_address_1) { ?>
                  <?php echo $payment_address_1; ?> ,
                  <?php } ?>
                  <?php if ($payment_city) { ?>
                  <?php echo $payment_city; ?> ,
                  <?php } ?>
                  <?php if ($payment_country) { ?>
                  <?php echo $payment_country; ?> -
                  <?php } ?>
                  <?php if ($payment_postcode) { ?>
                  <?php echo $payment_postcode; ?>
                  <?php } ?>
                </td>
              </tr>
              <tr>
                <td><b>GSTIN/UIN : </b><?php if ($company) { ?>
                  <?php echo $company; ?><br />
                  <?php } ?></td>
              </tr>
              <tr>
                <td><b>Mobile : </b>+91-<?php if ($website) { ?>
                  <?php echo $website; ?><br />
                  <?php } ?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <!-- <div class="row well">
        <div class="col-xs-6">
          <h5>Invoice No. : <?php echo $invoice_prefix; ?><?php echo $invoice_id; ?></h5>
          <?php echo $text_issued; ?> <?php echo $date_issued; ?><br />
          <?php if ($payment_firstname || $payment_lastname) { ?>
          <?php echo $payment_firstname; ?> <?php echo $payment_lastname; ?><br />
          <?php } else { ?>
          <?php echo $firstname; ?> <?php echo $lastname; ?><br />
          <?php } ?>
          <?php echo $email; ?><br />
          <?php if ($website) { ?>
          <?php echo $website; ?><br />
          <?php } ?>
          <?php if ($payment_company) { ?>
          <?php echo $payment_company; ?><br />
          <?php } elseif ($company) { ?>
          <?php echo $company; ?><br />
          <?php } ?>
          <?php if ($payment_address_1) { ?>
          <?php echo $payment_address_1; ?><br />
          <?php } ?>
          <?php if ($payment_address_2) { ?>
          <?php echo $payment_address_2; ?><br />
          <?php } ?>
          <?php if ($payment_city) { ?>
          <?php echo $payment_city; ?><br />
          <?php } ?>
          <?php if ($payment_postcode) { ?>
          <?php echo $payment_postcode; ?><br />
          <?php } ?>
          <?php if ($payment_country) { ?>
          <?php echo $payment_country; ?><br />
          <?php } ?>
          <?php if ($payment_zone) { ?>
          <?php echo $payment_zone; ?><br />
          <?php } ?>
        </div>
        <div class="col-xs-6 text-right">
          <h3><?php echo $system_company; ?></h3>
          <h5><b>GSTIN : 20BJEPA5588B1ZS </b></h5>
          <?php echo $system_address; ?><br />
          <?php echo $system_email; ?><br />
          <?php if ($system_telephone) { ?>
          <?php echo $system_telephone; ?><br />
          <?php } ?>
          <?php if ($system_fax) { ?>
          <?php echo $system_fax; ?><br />
          <?php } ?>
        </div>
      </div> -->
      <div class="row" style="border: 2px solid black;">
        <br>
        <div class="col-xs-12">
          <!-- <h3><?php echo $text_item; ?></h3> -->
          <table class="table table-striped table-bordered" style="margin-bottom: 0px;">
            <tr>
              <th class="text-left" style="width:10%;"><?php echo $column_number; ?></th>
              <th class="text-left" style="width:30%;"><?php echo $column_description; ?></th>
              <th class="text-left" style="width:10%;"> HSN/SAC </th>
              <th class="text-left" style="width:10%;"><?php echo $column_quantity; ?></th>
              <th class="text-right" style="width:10%;"><?php echo $column_price; ?></th>
              <th class="text-right" style="width:10%;"><?php echo $column_discount; ?></th>
              <th class="text-right" style="width:20%;">Value of Supply</th>
            </tr>
            <?php foreach ($items as $item) { ?>
            <tr>
              <td class="text-left"><?php echo $item['number']; ?></td>
              <td class="text-left">
                <b><?php echo $item['title']; ?></b><br />
                <?php echo $item['description']; ?>
              </td>
              <td class="text-left">8712</td>
              <td class="text-left"><?php echo $item['quantity']; ?> PC </td>
              <td class="text-right"><?php echo $item['price']; ?></td>
              <td class="text-right"><?php echo $item['discount']; ?></td>
              <td class="text-right"><?php echo $item['total']; ?></td>
            </tr>
            <?php } ?>
            <?php foreach ($totals as $total) { ?>
            <tr>
              <td class="text-right" colspan="6"><b><?php echo $total['title']; ?></b></td>
              <td class="text-right"><?php echo $total['text']; ?></td>
            </tr>
            <?php } ?>
          </table>
          <!-- <?php $gst = array_slice($totals, 1, 1); ?> -->
          <?php $taxable_value = array_slice($totals, 0, 1); ?>
          <?php
            $gstax = explode(".",$gst[0]['text']);
            $tax = explode(",",$gstax[1]);
            $gst = $tax[0].$tax[1];
            $cgst = (int)$gst / 2;
            $sgst = (int)$gst / 2;
            $tw = array_slice($totals, 2, 1);
            $total_word = explode(".",$tw[0]['text']);
            $t_word = explode(",",$total_word[1]);
            $word_total = $t_word[0].$t_word[1];
            $totalword = (int)$word_total;
          ?>

          <table class="table">
            <tr>
              <td>Amount Chargeable (in words)<br><b>
                <?php
                /**
                 * Created by PhpStorm.
                 * User: sakthikarthi
                 * Date: 9/22/14
                 * Time: 11:26 AM
                 * Converting Currency Numbers to words currency format
                 */
              $number = $totalword;
                 $no = round($number);
                 $point = round($number - $no, 2) * 100;
                 $hundred = null;
                 $digits_1 = strlen($no);
                 $i = 0;
                 $str = array();
                 $words = array('0' => '', '1' => 'One', '2' => 'Two',
                  '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
                  '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
                  '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
                  '13' => 'Thirteen', '14' => 'Fourteen',
                  '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
                  '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
                  '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
                  '60' => 'Sixty', '70' => 'Seventy',
                  '80' => 'Eighty', '90' => 'Ninety');
                 $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
                 while ($i < $digits_1) {
                   $divider = ($i == 2) ? 10 : 100;
                   $number = floor($no % $divider);
                   $no = floor($no / $divider);
                   $i += ($divider == 10) ? 1 : 2;
                   if ($number) {
                      $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                      $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                      $str [] = ($number < 21) ? $words[$number] .
                          " " . $digits[$counter] . $plural . " " . $hundred
                          :
                          $words[floor($number / 10) * 10]
                          . " " . $words[$number % 10] . " "
                          . $digits[$counter] . $plural . " " . $hundred;
                   } else $str[] = null;
                }
                $str = array_reverse($str);
                $result = implode('', $str);
                $points = ($point) ?
                  "." . $words[$point / 10] . " " .
                        $words[$point = $point % 10] : '';
                echo $result . "Rupees  " ;
               ?> </b>
              </td>
            </tr>
            <tr></tr>
            <tr style="text-align:center">
              <td>HSN/SAC</td>
              <td>Taxable Value</td>
              <td>Central Tax<br> <b>CGST @ 6%</b></td>
              <td>State Tax<br> <b>SGST @ 6%</b></td>
              <td>Total Tax Amount<br> <b>GST @ 12%</b></td>
            </tr>
            <tr style="text-align:center">
              <td>8517</td>
              <td><?php echo $taxable_value[0]['text']; ?></td>
              <td>Rs.<?php echo $cgst ?></td>
              <td>Rs.<?php echo $sgst ?></td>
              <td>Rs.<?php echo $gst ?></td>
            </tr>

          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-6" style="border: 2px solid black;height:200px;">
          <br>
          <center><b>Signature of the Buyer </b></center>
          <br>
          <br>
          <br>
        </div>
        <div class="col-xs-6" style="border: 2px solid black;height:200px;border-left: 0px;">
          <br>
          <center><i>Certified that the particular given above are true and correct</i></center>
          <br>
          <center><h5><b>For Divertido Invoices</b></h5></center>
          <br>
          <br>
          <br>
          <br>
          <center><h5><b>Authorised Signatory</b></h5></center>
        </div>
      </div>
      <br>
      <div class="row">
          <div class="col-xs-12">
            <center>This is a Computer Generated Invoice</center>
          </div>
      </div>
    </div>

  </body>
</html>
