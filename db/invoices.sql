-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 14, 2017 at 08:35 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `invoices`
--

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_account`
--

CREATE TABLE `vfcst_account` (
  `account_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `type` varchar(32) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `retained_earnings` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_account`
--

INSERT INTO `vfcst_account` (`account_id`, `name`, `description`, `type`, `parent_id`, `status`, `retained_earnings`) VALUES
(2100, 'Accounts Payable', '', 'liability', 0, 1, 0),
(1100, 'Cash', '', 'current_asset', 0, 1, 0),
(1200, 'Accounts Receivable', '', 'current_asset', 0, 1, 0),
(5300, 'Tax Expense', '', 'expense', 0, 1, 0),
(2200, 'Unearned Revenue', '', 'liability', 0, 1, 0),
(3200, 'Retained Earnings', 'Do not use. Do not delete. Do not disable.', 'equity', 0, 1, 1),
(3100, 'Share Capital', '', 'equity', 0, 1, 0),
(5201, 'Sales Staff', '', 'expense', 5200, 1, 0),
(5100, 'Office Equipment', '', 'expense', 0, 1, 0),
(1300, 'Office Equipment', '', 'fixed_asset', 0, 1, 0),
(5200, 'Salaries', '', 'expense', 0, 1, 0),
(4100, 'Shop Sales', '', 'sale', 0, 1, 0),
(5202, 'Finance Staff', '', 'expense', 5200, 1, 0),
(4200, 'Online Sales', '', 'revenue', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_activity`
--

CREATE TABLE `vfcst_activity` (
  `activity_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_activity`
--

INSERT INTO `vfcst_activity` (`activity_id`, `message`, `date_added`) VALUES
(1, '[Admin] admin logged in.', '2017-09-13 22:32:22'),
(2, '[Admin] admin logged in.', '2017-09-13 23:05:48'),
(3, '[Admin] mahtab logged in.', '2017-09-13 23:06:05'),
(4, '[Admin] admin logged in.', '2017-09-13 23:08:29'),
(5, '[Admin] mahtab logged in.', '2017-09-13 23:09:25'),
(6, '[Admin] admin logged in.', '2017-09-13 23:14:35'),
(7, '[Admin] admin logged in.', '2017-09-13 23:25:55'),
(8, '[Admin] admin logged in.', '2017-09-13 23:37:33'),
(9, '[Admin] admin logged in.', '2017-09-14 11:44:43');

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_article`
--

CREATE TABLE `vfcst_article` (
  `article_id` int(11) NOT NULL,
  `top` tinyint(1) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_article`
--

INSERT INTO `vfcst_article` (`article_id`, `top`, `parent_id`, `sort_order`, `status`) VALUES
(1, 1, 0, 0, 1),
(2, 1, 1, 1, 1),
(3, 1, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_article_description`
--

CREATE TABLE `vfcst_article_description` (
  `article_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_article_description`
--

INSERT INTO `vfcst_article_description` (`article_id`, `language_id`, `title`, `description`) VALUES
(2, 1, 'History', '&lt;h2&gt;History&lt;/h2&gt;My Company was founded in 2000.&lt;br&gt;'),
(3, 1, 'How to Pay', '&lt;h2&gt;How to Pay&lt;/h2&gt;Please login to pay for your invoices. You can login by clicking the \'login\' link and proceed to the \'my invoices\' page to view the invoices you have. There, you can make payment for the invoices you have.&lt;br&gt;'),
(1, 1, 'About Us', '&lt;p&gt;&lt;h2&gt;About Us&lt;/h2&gt;My Company specialises in software and web applications. Founded in \r\n2000, My Company has grown to a team of 20 developers over the years. We\r\n love the web, and we are dedicated to bring out the best in all our \r\nwork.&lt;/p&gt;&lt;p&gt;We provide many other services such as office networking \r\nand mobile application development. You can get in touch with us through\r\n the \'contact us\' link above.&lt;/p&gt;');

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_blog_category`
--

CREATE TABLE `vfcst_blog_category` (
  `blog_category_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_blog_category`
--

INSERT INTO `vfcst_blog_category` (`blog_category_id`, `parent_id`, `sort_order`, `status`) VALUES
(1, 0, 0, 1),
(2, 1, 0, 1),
(3, 2, 0, 1),
(4, 1, 0, 1),
(5, 0, 0, 1),
(6, 5, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_blog_category_description`
--

CREATE TABLE `vfcst_blog_category_description` (
  `blog_category_description_id` int(11) NOT NULL,
  `blog_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keyword` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_blog_category_description`
--

INSERT INTO `vfcst_blog_category_description` (`blog_category_description_id`, `blog_category_id`, `language_id`, `name`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(4, 1, 1, 'Tech News', 'Tech News', '', ''),
(5, 2, 1, 'Mobile', 'Mobile', '', ''),
(6, 3, 1, '4 Inch', '4 Inch', '', ''),
(7, 4, 1, 'Desktop', 'Desktop', '', ''),
(8, 5, 1, 'Company News', 'Company News', '', ''),
(9, 6, 1, 'New Products', 'New Products', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_blog_post`
--

CREATE TABLE `vfcst_blog_post` (
  `blog_post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `view` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_blog_post`
--

INSERT INTO `vfcst_blog_post` (`blog_post_id`, `user_id`, `view`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(1, 1, 0, 0, 1, '2015-06-02 15:11:28', '2016-03-29 14:17:42'),
(2, 1, 1, 0, 1, '2016-03-29 14:20:01', '2016-03-29 14:20:01');

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_blog_post_description`
--

CREATE TABLE `vfcst_blog_post_description` (
  `blog_post_description_id` int(11) NOT NULL,
  `blog_post_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keyword` text NOT NULL,
  `short_description` text NOT NULL,
  `description` text NOT NULL,
  `tag` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_blog_post_description`
--

INSERT INTO `vfcst_blog_post_description` (`blog_post_description_id`, `blog_post_id`, `language_id`, `image`, `title`, `meta_title`, `meta_description`, `meta_keyword`, `short_description`, `description`, `tag`) VALUES
(2, 1, 1, '', 'My First Post', 'My First Post', '', '', 'Hello world, this is my first blog post.&lt;br&gt;', '&lt;p&gt;Hello world, this is my first blog post.&lt;/p&gt;&lt;p&gt;&lt;br&gt;Hello everyone and thank you for reading our first post on this blog.&lt;br&gt;&lt;/p&gt;', 'invoice, logic invoice'),
(3, 2, 1, '', 'Cool Phone 2.0', 'Cool Phone 2.0', '', '', 'We present to you, Cool Phone 2.0!&lt;br&gt;', 'Cool Phone 2.0 is My Company latest mobile phone. Read on to find out more details.&lt;br&gt;', '');

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_blog_post_to_blog_category`
--

CREATE TABLE `vfcst_blog_post_to_blog_category` (
  `blog_post_id` int(11) NOT NULL,
  `blog_category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_blog_post_to_blog_category`
--

INSERT INTO `vfcst_blog_post_to_blog_category` (`blog_post_id`, `blog_category_id`) VALUES
(1, 5),
(2, 1),
(2, 2),
(2, 5),
(2, 6);

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_currency`
--

CREATE TABLE `vfcst_currency` (
  `currency_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` decimal(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_currency`
--

INSERT INTO `vfcst_currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(3, 'Indian Rupee', 'INR', 'Rs.', '', '0', '1.00000000', 1, '2017-09-14 10:39:48');

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_customer`
--

CREATE TABLE `vfcst_customer` (
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `email` varchar(96) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `password` varchar(40) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `token` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_customer`
--

INSERT INTO `vfcst_customer` (`customer_id`, `firstname`, `lastname`, `company`, `website`, `email`, `salt`, `password`, `ip`, `token`, `status`, `date_added`, `date_modified`) VALUES
(1, 'Mahtab', 'Alam', 'Divertido', 'www.divertido.xyz', 'mahtab.ceh@gmail.com', 'ed2305bd0', 'c8383c008c56c807c167e0ab17ed4b724fff7643', '', '', 1, '2017-09-13 22:33:42', '2017-09-13 22:33:42'),
(2, 'Sunil', 'Kumar', 'Fentrogreen', 'www.fentrogreen.com', 'info@fentogreen.com', '2da54acf7', 'a5bcd8b6c705371ce45a6e9c8765d8fe91e56dea', '', '', 1, '2017-09-14 10:15:42', '2017-09-14 10:15:42');

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_customer_credit`
--

CREATE TABLE `vfcst_customer_credit` (
  `customer_credit_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `description` text NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_customer_ip`
--

CREATE TABLE `vfcst_customer_ip` (
  `customer_ip_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_email_template`
--

CREATE TABLE `vfcst_email_template` (
  `email_template_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `priority` int(3) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `email` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_email_template`
--

INSERT INTO `vfcst_email_template` (`email_template_id`, `type`, `priority`, `status`, `email`) VALUES
(1, 'new_customer_admin', 0, 1, ''),
(2, 'new_customer_customer', 0, 1, ''),
(3, 'edit_customer_admin', 0, 1, ''),
(4, 'edit_customer_customer', 0, 1, ''),
(5, 'new_credit_admin', 0, 1, ''),
(6, 'new_credit_customer', 0, 1, ''),
(7, 'new_invoice_admin', 0, 1, ''),
(8, 'new_invoice_customer', 0, 1, ''),
(9, 'edit_invoice_admin', 0, 1, ''),
(10, 'edit_invoice_customer', 0, 1, ''),
(11, 'new_recurring_admin', 0, 1, ''),
(12, 'new_recurring_customer', 0, 1, ''),
(13, 'edit_recurring_admin', 0, 1, ''),
(14, 'edit_recurring_customer', 0, 1, ''),
(15, 'new_transaction_admin', 0, 1, ''),
(16, 'edit_transaction_admin', 0, 1, ''),
(17, 'forgotten_password_admin', 0, 1, ''),
(18, 'forgotten_password_customer', 0, 1, ''),
(19, 'status_5', 0, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_email_template_description`
--

CREATE TABLE `vfcst_email_template_description` (
  `email_template_description_id` int(11) NOT NULL,
  `email_template_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `html` text NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_email_template_description`
--

INSERT INTO `vfcst_email_template_description` (`email_template_description_id`, `email_template_id`, `language_id`, `subject`, `html`, `text`) VALUES
(1, 1, 1, '[{website_name}] New Account Registration', '&lt;p&gt;Hi there,&lt;/p&gt;&lt;p&gt;A new account was recently created with the following details:&lt;/p&gt;&lt;p&gt;Customer ID: {customer_id}&lt;br&gt;First Name: {firstname}&lt;br&gt;Last Name: {lastname}&lt;br&gt;Company: {company}&lt;br&gt;Email: {email}&lt;/p&gt;&lt;p&gt;Regards,&lt;br&gt;{website_name}&lt;br&gt;&lt;/p&gt;', 'Hi there,\r\n\r\nA new account was recently created with the following details:\r\n\r\nCustomer ID: {customer_id}\r\nFirst Name: {firstname}\r\nLast Name: {lastname}\r\nCompany: {company}\r\nEmail: {email}\r\n\r\nRegards,\r\n{website_name}'),
(2, 2, 1, 'Welcome to {website_name}', '&lt;p&gt;Hi {firstname},&lt;/p&gt;&lt;p&gt;Welcome to {website_name}. You may login to your account by clicking &lt;a href=&quot;{website_url}index.php?load=account/login&quot; target=&quot;_blank&quot;&gt;here&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;Your password is {password}.&lt;/p&gt;&lt;p&gt;Regards,&lt;br&gt;{website_name}&lt;/p&gt;', 'Hi {firstname},\r\n\r\nWelcome to {website_name}. You may login to your account at the following URL: {website_url}index.php?load=account/login\r\n\r\nYour password is {password}.\r\n\r\nRegards,\r\n{website_name}'),
(3, 5, 1, '[{website_name}] Credit Added', '&lt;p&gt;Hi there,&lt;br&gt;&lt;/p&gt;&lt;p&gt;{firstname}\'s account was recently credited:&lt;br&gt;&lt;/p&gt;&lt;p&gt;Customer ID: {customer_id}&lt;br&gt;Amount: {amount}&lt;br&gt;Description: {description}&lt;br&gt;Date Added: {date_added}&lt;/p&gt;&lt;p&gt;Regards,&lt;br&gt;{website_name}&lt;br&gt;&lt;/p&gt;', 'Hi there,\r\n\r\n{firstname}\'s account was recently credited:\r\n\r\nCustomer ID: {customer_id}\r\nAmount: {amount}\r\nDescription: {description}\r\nDate Added: {date_added}\r\n\r\nRegards,\r\n{website_name}'),
(4, 6, 1, '[{website_name}] Your have new credits', '&lt;p&gt;Hi {firstname},&lt;/p&gt;&lt;p&gt;We have just added credits to your account.&lt;/p&gt;&lt;p&gt;Amount: {amount}&lt;br&gt;Description: {description}&lt;br&gt;Date Added: {date_added}&lt;/p&gt;&lt;p&gt;You can view all your available credits by clicking &lt;a target=&quot;_blank&quot; href=&quot;{website_url}index.php?load=account/credit&quot;&gt;here&lt;/a&gt;.&lt;br&gt;&lt;/p&gt;&lt;p&gt;Regards,&lt;br&gt;{website_name}&lt;/p&gt;', 'Hi {firstname},\r\n\r\nWe have just added credits to your account.\r\n\r\nAmount: {amount}\r\nDescription: {description}\r\nDate Added: {date_added}\r\n\r\nYou can view all your available credits by clicking on the following link: {website_url}index.php?load=account/credit\r\n\r\nRegards,\r\n{website_name}'),
(5, 10, 1, '[{website_name}] Invoice #{invoice_id} Updated', '&lt;p&gt;Hi {firstname},&lt;/p&gt;&lt;p&gt;Your invoice #{invoice_id} was recently updated.&lt;/p&gt;&lt;p&gt;Status: {status}&lt;br&gt;Total: {total}&lt;br&gt;Date Issued: {date_issued}&lt;br&gt;Date Due: {date_due}&lt;br&gt;Date Modified: {date_modified}&lt;/p&gt;&lt;p&gt;You can view all your invoices by clicking &lt;a target=&quot;_blank&quot; href=&quot;{website_url}index.php?load=account/invoice&quot;&gt;here&lt;/a&gt;.&lt;br&gt;&lt;/p&gt;&lt;p&gt;Regards,&lt;br&gt;{website_name}&lt;br&gt;&lt;/p&gt;', 'Hi {firstname},\r\n\r\nYour invoice #{invoice_id} was recently updated.\r\n\r\nStatus: {status}\r\nTotal: {total}\r\nDate Issued: {date_issued}\r\nDate Due: {date_due}\r\nDate Modified: {date_modified}\r\n\r\nYou can view all your invoices by clicking on the following link: {website_url}index.php?load=account/invoice\r\n\r\nRegards,\r\n{website_name}'),
(6, 11, 1, '[{website_name}] New Recurring Payment #{recurring_id}', '&lt;p&gt;Hi there,&lt;br&gt;&lt;/p&gt;&lt;p&gt;Recurring payment #{recurring_id} was recently added with the following details:&lt;/p&gt;&lt;p&gt;Cycle: {cycle}&lt;br&gt;Recurring Payment ID: {recurring_id}&lt;br&gt;Total: {total}&lt;br&gt;Customer ID: {customer_id}&lt;br&gt;First Name: {firstname}&lt;br&gt;Date Added: {date_added}&lt;br&gt;Date Due: {date_due}&lt;br&gt;&lt;/p&gt;&lt;p&gt;Regards,&lt;br&gt;{website_name}&lt;br&gt;&lt;/p&gt;', 'Hi there,\r\n\r\nRecurring payment #{recurring_id} was recently added with the following details:\r\n\r\nCycle: {cycle}\r\nRecurring Payment ID: {recurring_id}\r\nTotal: {total}\r\nCustomer ID: {customer_id}\r\nFirst Name: {firstname}\r\nDate Added: {date_added}\r\nDate Due: {date_due}\r\n\r\nRegards,\r\n{website_name}'),
(7, 12, 1, '[{website_name}] New Recurring Payment #{recurring_id}', '&lt;p&gt;Hi {firstname},&lt;/p&gt;&lt;p&gt;Recurring payment #{recurring_id} was recently added to your account with the following details:&lt;/p&gt;&lt;p&gt;Cycle: {cycle}&lt;br&gt;Status: {status}&lt;br&gt;Recurring Payment ID: {recurring_id}&lt;br&gt;Total: {total}&lt;br&gt;Date Added: {date_added}&lt;br&gt;Date Due: {date_due}&lt;/p&gt;&lt;p&gt;You can view all your recurring payments by clicking &lt;a target=&quot;_blank&quot; href=&quot;{website_url}index.php?load=account/recurring&quot;&gt;here&lt;/a&gt;.&lt;br&gt;&lt;/p&gt;&lt;p&gt;Regards,&lt;br&gt;{website_name}&lt;br&gt;&lt;/p&gt;', 'Hi {firstname},\r\n\r\nRecurring payment #{recurring_id} was recently added to your account with the following details:\r\n\r\nCycle: {cycle}\r\nStatus: {status}\r\nRecurring Payment ID: {recurring_id}\r\nTotal: {total}\r\nDate Added: {date_added}\r\nDate Due: {date_due}\r\n\r\nYou can view all your recurring payments by clicking on the following link: {website_url}index.php?load=account/recurring\r\n\r\nRegards,\r\n{website_name}'),
(8, 13, 1, '[{website_name}] Recurring Payment #{recurring_id} Updated', '&lt;p&gt;Hi there,&lt;/p&gt;&lt;p&gt;Recurring payment #{recurring_id} was recently updated.&lt;/p&gt;&lt;p&gt;Cycle: {cycle}&lt;br&gt;Status: {status}&lt;br&gt;Total: {total}&lt;br&gt;Date Added: {date_added}&lt;br&gt;Date Due: {date_due}&lt;br&gt;Date Modified: {date_modified}&lt;/p&gt;&lt;p&gt;Regards,&lt;br&gt;{website_name}&lt;br&gt;&lt;/p&gt;', 'Hi there,\r\n\r\nRecurring payment #{recurring_id} was recently updated.\r\n\r\nCycle: {cycle}\r\nStatus: {status}\r\nTotal: {total}\r\nDate Added: {date_added}\r\nDate Due: {date_due}\r\nDate Modified: {date_modified}\r\n\r\nRegards,\r\n{website_name}'),
(9, 15, 1, '[{website_name}] New Transaction', '&lt;p&gt;Hi there,&lt;br&gt;&lt;/p&gt;&lt;p&gt;A new transaction was recently added:&lt;/p&gt;&lt;p&gt;Linked Invoice ID: {invoice_id}&lt;br&gt;Date: {date}&lt;br&gt;Date Added: {date_added}&lt;br&gt;Date Modified: {date_modified}&lt;/p&gt;&lt;p&gt;Regards,&lt;br&gt;{website_name}&lt;br&gt;&lt;/p&gt;', 'Hi there,\r\n\r\nA new transaction was recently added:\r\n\r\nLinked Invoice ID: {invoice_id}\r\nDate: {date}\r\nDate Added: {date_added}\r\nDate Modified: {date_modified}\r\n\r\nRegards,\r\n{website_name}'),
(10, 16, 1, '[{website_name}] Transaction Updated', '&lt;p&gt;Hi there,&lt;br&gt;&lt;/p&gt;&lt;p&gt;Someone recently updated a transaction:&lt;/p&gt;&lt;p&gt;Linked Invoice ID: {invoice_id}&lt;br&gt;Date: {date}&lt;br&gt;Date Added: {date_added}&lt;br&gt;Date Modified: {date_modified}&lt;/p&gt;&lt;p&gt;Regards,&lt;br&gt;{website_name}&lt;br&gt;&lt;/p&gt;', 'Hi there,\r\n\r\nSomeone recently updated a transaction:\r\n\r\nLinked Invoice ID: {invoice_id}\r\nDate: {date}\r\nDate Added: {date_added}\r\nDate Modified: {date_modified}\r\n\r\nRegards,\r\n{website_name}'),
(11, 3, 1, '[{website_name}] Customer Account Updated', '&lt;p&gt;Hi there,&lt;/p&gt;&lt;p&gt;A customer account was updated with the following details:&lt;/p&gt;&lt;p&gt;Customer ID: {customer_id}&lt;br&gt;First Name: {firstname}&lt;br&gt;Last Name: {lastname}&lt;br&gt;Company: {company}&lt;br&gt;Website: {website}&lt;br&gt;Email: {email}&lt;br&gt;Status: {status}&lt;br&gt;&lt;/p&gt;&lt;p&gt;Regards,&lt;br&gt;{website_name}&lt;br&gt;&lt;/p&gt;', 'Hi there,\r\n\r\nA customer account was updated with the following details:\r\n\r\nCustomer ID: {customer_id}\r\nFirst Name: {firstname}\r\nLast Name: {lastname}\r\nCompany: {company}\r\nWebsite: {website}\r\nEmail: {email}\r\nStatus: {status}\r\n\r\nRegards,\r\n{website_name}'),
(12, 4, 1, '[{website_name}] Account Updated', '&lt;p&gt;Hi {firstname},&lt;/p&gt;&lt;p&gt;Your account was recently updated with the following details:&lt;br&gt;&lt;/p&gt;&lt;p&gt;First Name: {firstname}&lt;br&gt;Last Name: {lastname}&lt;br&gt;Company: {company}&lt;br&gt;Website: {website}&lt;br&gt;Email: {email}&lt;br&gt;Status: {status}&lt;br&gt;&lt;/p&gt;&lt;p&gt;Regards,&lt;br&gt;{website_name}&lt;/p&gt;', 'Hi {firstname},\r\n\r\nYour account was recently updated with the following details:\r\n\r\nFirst Name: {firstname}\r\nLast Name: {lastname}\r\nCompany: {company}\r\nWebsite: {website}\r\nEmail: {email}\r\nStatus: {status}\r\n\r\nRegards,\r\n{website_name}'),
(13, 9, 1, '[{website_name}] Invoice #{invoice_id} Updated', '&lt;p&gt;Hi there,&lt;/p&gt;&lt;p&gt;Invoice #{invoice_id} was recently updated.&lt;/p&gt;&lt;p&gt;Status: {status}&lt;br&gt;Total: {total}&lt;br&gt;Date Issued: {date_issued}&lt;br&gt;Date Due: {date_due}&lt;br&gt;Date Modified: {date_modified}&lt;/p&gt;&lt;p&gt;Regards,&lt;br&gt;{website_name}&lt;br&gt;&lt;/p&gt;', 'Hi there,\r\n\r\nInvoice #{invoice_id} was recently updated.\r\n\r\nStatus: {status}\r\nTotal: {total}\r\nDate Issued: {date_issued}\r\nDate Due: {date_due}\r\nDate Modified: {date_modified}\r\n\r\nRegards,\r\n{website_name}'),
(14, 14, 1, '[{website_name}] Recurring Payment #{recurring_id} Updated', '&lt;p&gt;Hi {firstname},&lt;/p&gt;\r\n&lt;p&gt;Your recurring payment #{recurring_id} was recently updated.&lt;/p&gt;\r\n&lt;p&gt;Cycle: {cycle}&lt;br&gt;Status: {status}&lt;br&gt;Total: {total}&lt;br&gt;Date Added: {date_added}&lt;br&gt;Date Due: {date_due}&lt;br&gt;Date Modified: {date_modified}&lt;/p&gt;\r\n&lt;p&gt;You can view all your recurring payments by clicking &lt;a target=&quot;_blank&quot; href=&quot;{website_url}index.php?load=account/recurring&quot;&gt;here&lt;/a&gt;.&lt;br&gt;&lt;/p&gt;\r\n&lt;p&gt;Regards,&lt;br&gt;{website_name}&lt;br&gt;&lt;/p&gt;', 'Hi {firstname},\r\n\r\nYour recurring payment #{recurring_id} was recently updated.\r\n\r\nCycle: {cycle}\r\nStatus: {status}\r\nTotal: {total}\r\nDate Added: {date_added}\r\nDate Due: {date_due}\r\nDate Modified: {date_modified}\r\n\r\nYou can view all your recurring payments by clicking on the following link: {website_url}index.php?load=account/recurring\r\n\r\nRegards,\r\n{website_name}'),
(15, 7, 1, '[{website_name}] New Invoice #{invoice_id}', '&lt;p&gt;Hi there,&lt;br&gt;&lt;/p&gt;&lt;p&gt;Invoice #{invoice_id} was recently added with the following details:&lt;/p&gt;&lt;p&gt;Status: {status}&lt;br&gt;Invoice ID: {invoice_id}&lt;br&gt;Total: {total}&lt;br&gt;Customer ID: {customer_id}&lt;br&gt;First Name: {firstname}&lt;br&gt;Date Added: {date_issued}&lt;br&gt;Date Due: {date_due}&lt;br&gt;&lt;/p&gt;&lt;p&gt;Regards,&lt;br&gt;{website_name}&lt;br&gt;&lt;/p&gt;', 'Hi there,\r\n\r\nInvoice #{invoice_id} was recently added with the following details:\r\n\r\nStatus: {status}\r\nInvoice ID: {invoice_id}\r\nTotal: {total}\r\nCustomer ID: {customer_id}\r\nFirst Name: {firstname}\r\nDate Added: {date_issued}\r\nDate Due: {date_due}\r\n\r\nRegards,\r\n{website_name}'),
(16, 8, 1, '[{website_name}] New Invoice #{invoice_id}', '&lt;p&gt;Hi {firstname},&lt;/p&gt;&lt;p&gt;Invoice #{invoice_id} was recently added to your account with the following details:&lt;/p&gt;&lt;p&gt;Status: {status}&lt;br&gt;Invoice ID: {invoice_id}&lt;br&gt;Total: {total}&lt;br&gt;Date Issued: {date_issued}&lt;br&gt;Date Due: {date_due}&lt;/p&gt;&lt;p&gt;You can view all your invoices by clicking &lt;a target=&quot;_blank&quot; href=&quot;{website_url}index.php?load=account/invoice&quot;&gt;here&lt;/a&gt;.&lt;br&gt;&lt;/p&gt;&lt;p&gt;Regards,&lt;br&gt;{website_name}&lt;br&gt;&lt;/p&gt;', 'Hi {firstname},\r\n\r\nInvoice #{invoice_id} was recently added to your account with the following details:\r\n\r\nStatus: {status}\r\nInvoice ID: {invoice_id}\r\nTotal: {total}\r\nDate Issued: {date_issued}\r\nDate Due: {date_due}\r\n\r\nYou can view all your invoices by clicking on the following link: {website_url}index.php?load=account/invoice\r\n\r\nRegards,\r\n{website_name}'),
(17, 17, 1, 'Password Change Request at {website_name}', '&lt;p&gt;Hi there,&lt;/p&gt;&lt;p&gt;Someone recently requested to change your account password. If you made the request, you can change your password by clicking &lt;a target=&quot;_blank&quot; href=&quot;{reset_link}&quot;&gt;here&lt;/a&gt;.&lt;/p&gt;&lt;p&gt;The password reset was requested from {ip}&lt;br&gt;&lt;/p&gt;&lt;p&gt;Regards,&lt;br&gt;{website_name}&lt;br&gt;&lt;/p&gt;', 'Hi there,\r\n\r\nSomeone recently requested to change your account password. If you made the request, you can change your password by clicking on the following link: {reset_link}\r\n\r\nThe password reset was requested from {ip}\r\n\r\nRegards,\r\n{website_name}'),
(18, 18, 1, 'Password Change Request at {website_name}', '&lt;p&gt;Hi {firstname},&lt;/p&gt;&lt;p&gt;Someone recently requested to change your account password. Your account password has been changed to the following:&lt;/p&gt;&lt;p&gt;{password}&lt;/p&gt;&lt;p&gt;The password change was requested from {ip}. If you did not make the change, please inform us immediately.&lt;/p&gt;&lt;p&gt;Regards,&lt;br&gt;{website_name}&lt;br&gt;&lt;/p&gt;', 'Hi {firstname},\r\n\r\nSomeone recently requested to change your account password. Your account password has been changed to the following:\r\n\r\n{password}\r\n\r\nThe password change was requested from {ip}. If you did not make the change, please inform us immediately.\r\n\r\nRegards,\r\n{website_name}'),
(19, 19, 1, 'Pending', '&lt;p&gt;Pending invoice&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;&lt;p&gt;{history_comment}&lt;/p&gt;', 'Pending invoice');

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_email_template_type`
--

CREATE TABLE `vfcst_email_template_type` (
  `email_template_type_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `variables` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_email_template_type`
--

INSERT INTO `vfcst_email_template_type` (`email_template_type_id`, `type`, `variables`) VALUES
(1, 'new_customer_admin', 'website_name, website_url, customer_id, firstname, lastname, company, website, email, status'),
(2, 'new_customer_customer', 'website_name, website_url, customer_id, firstname, lastname, company, website, email, status, password'),
(3, 'edit_customer_admin', 'website_name, website_url, customer_id, firstname, lastname, company, website, email, status'),
(4, 'edit_customer_customer', 'website_name, website_url, customer_id, firstname, lastname, company, website, email, status'),
(5, 'new_credit_admin', 'website_name, website_url, customer_id, firstname, lastname, company, website, email, amount, description, date_added'),
(6, 'new_credit_customer', 'website_name, website_url, customer_id, firstname, lastname, company, website, email, amount, description, date_added'),
(7, 'new_invoice_admin', 'website_name, website_url, customer_id, firstname, lastname, company, website, email, invoice_id, comment, total, status, payment_name, date_issued, date_due, date_modified'),
(8, 'new_invoice_customer', 'website_name, website_url, customer_id, firstname, lastname, company, website, email, invoice_id, comment, total, status, payment_name, date_issued, date_due, date_modified'),
(9, 'edit_invoice_admin', 'website_name, website_url, customer_id, firstname, lastname, company, website, email, invoice_id, comment, total, status, payment_name, date_issued, date_due, date_modified'),
(10, 'edit_invoice_customer', 'website_name, website_url, customer_id, firstname, lastname, company, website, email, invoice_id, comment, total, status, payment_name, date_issued, date_due, date_modified'),
(11, 'new_recurring_admin', 'website_name, website_url, customer_id, firstname, lastname, company, website, email, recurring_id, comment, total, status, cycle, payment_name, date_added, date_due, date_modified'),
(12, 'new_recurring_customer', 'website_name, website_url, customer_id, firstname, lastname, company, website, email, recurring_id, comment, total, status, cycle, payment_name, date_added, date_due, date_modified'),
(13, 'edit_recurring_admin', 'website_name, website_url, customer_id, firstname, lastname, company, website, email, recurring_id, comment, total, status, cycle, payment_name, date_added, date_due, date_modified'),
(14, 'edit_recurring_customer', 'website_name, website_url, customer_id, firstname, lastname, company, website, email, recurring_id, comment, total, status, cycle, payment_name, date_added, date_due, date_modified'),
(15, 'new_transaction_admin', 'website_name, website_url, invoice_id, date, date_added, date_modified'),
(16, 'edit_transaction_admin', 'website_name, website_url, invoice_id, date, date_added, date_modified'),
(17, 'forgotten_password_admin', 'website_name, website_url, email, reset_link, ip'),
(18, 'forgotten_password_customer', 'website_name, website_url, firstname, lastname, email, password, ip'),
(19, 'status', 'website_name, website_url, customer_id, firstname, lastname, company, website, email, invoice_id, comment, history_comment, total, status, payment_name, date_issued, date_due, date_modified');

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_extension`
--

CREATE TABLE `vfcst_extension` (
  `extension` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_extension`
--

INSERT INTO `vfcst_extension` (`extension`, `type`, `code`) VALUES
(2, 'total', 'total'),
(3, 'total', 'sub_total'),
(4, 'payment', 'cheque'),
(5, 'total', 'tax'),
(6, 'payment', 'bank_transfer'),
(7, 'module', 'contact_form'),
(8, 'payment', 'credit');

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_inventory`
--

CREATE TABLE `vfcst_inventory` (
  `inventory_id` int(11) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `cost` decimal(15,4) NOT NULL,
  `sell` decimal(15,4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_inventory`
--

INSERT INTO `vfcst_inventory` (`inventory_id`, `sku`, `name`, `description`, `image`, `quantity`, `cost`, `sell`, `status`, `date_added`, `date_modified`) VALUES
(1, 'L513030', 'Levis 513 Size 30', 'The Levi\'s® brand epitomizes classic American style and effortless cool. Our range of clothing for men and women is available in more than 110 countries.', '', 100, '700.0000', '950.0000', 1, '2017-09-13 22:38:44', '2017-09-13 22:38:44'),
(2, 'L513032', 'Levis 513 Size 32', 'The Levi\'s® brand epitomizes classic American style and effortless cool. Our range of clothing for men and women is available in more than 110 countries.', '', 100, '150.0000', '500.0000', 1, '2017-09-13 23:10:20', '2017-09-13 23:10:20');

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_invoice`
--

CREATE TABLE `vfcst_invoice` (
  `invoice_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `email` varchar(96) NOT NULL,
  `payment_firstname` varchar(32) NOT NULL,
  `payment_lastname` varchar(32) NOT NULL,
  `payment_company` varchar(128) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(10) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `payment_code` varchar(255) NOT NULL,
  `payment_name` varchar(255) NOT NULL,
  `payment_description` text NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL DEFAULT '1.00000000',
  `comment` text NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '0',
  `transaction` tinyint(1) NOT NULL DEFAULT '0',
  `date_due` date NOT NULL DEFAULT '0000-00-00',
  `date_issued` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_invoice`
--

INSERT INTO `vfcst_invoice` (`invoice_id`, `recurring_id`, `customer_id`, `firstname`, `lastname`, `company`, `website`, `email`, `payment_firstname`, `payment_lastname`, `payment_company`, `payment_address_1`, `payment_address_2`, `payment_city`, `payment_postcode`, `payment_country`, `payment_zone`, `total`, `payment_code`, `payment_name`, `payment_description`, `currency_code`, `currency_value`, `comment`, `status_id`, `transaction`, `date_due`, `date_issued`, `date_modified`) VALUES
(1, 0, 1, 'Mahtab', 'Alam', '', '', 'mahtab.ceh@gmail.com', '', '', '', '', '', '', '', '', '', '1016.5000', 'cheque', 'BOI 123456', '', 'INR', '1.00000000', '', 4, 0, '2017-09-15', '2017-09-13 22:46:06', '2017-09-13 22:59:07'),
(2, 0, 1, 'Mahtab', 'Alam', 'Divertido', 'www.divertido.xyz', 'mahtab.ceh@gmail.com', 'Mahtab', 'Alam', 'Divertido', 'Hindpiri', '', 'Ranchi', '834002', 'India', '', '6080.0000', 'cheque', 'BOB 23009', '', 'INR', '1.00000000', '', 4, 0, '2017-09-13', '2017-09-13 22:57:03', '2017-09-13 22:58:59'),
(3, 0, 1, 'Mahtab', 'Alam', 'Divertido', 'www.divertido.xyz', 'mahtab.ceh@gmail.com', 'Mahtab', 'Alam', 'Divertido', '', '', '', '', '', '', '3475.5000', 'bank_transfer', 'BOI 12345', 'Paid Advance', 'INR', '1.00000000', '', 1, 0, '2017-09-13', '2017-09-13 23:13:00', '2017-09-13 23:13:00'),
(4, 0, 2, 'Sunil', 'Kumar', 'Fentrogreen', 'www.fentrogreen.com', 'info@fentogreen.com', '', '', '', '', '', '', '', '', '', '4700.0000', 'bank_transfer', 'Transfer to SBI', 'A/C No. 32779677252\r\nName : Aftab Alam\r\nIFSC: SBIN0003154', 'INR', '1.00000000', '', 5, 0, '2017-09-14', '2017-09-14 10:15:42', '2017-09-14 10:15:42');

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_invoice_history`
--

CREATE TABLE `vfcst_invoice_history` (
  `invoice_history_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_invoice_history`
--

INSERT INTO `vfcst_invoice_history` (`invoice_history_id`, `invoice_id`, `status_id`, `comment`, `date_added`) VALUES
(1, 1, 5, '', '2017-09-13 22:46:16'),
(2, 2, 4, '', '2017-09-13 22:58:59'),
(3, 1, 4, '', '2017-09-13 22:59:07');

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_invoice_item`
--

CREATE TABLE `vfcst_invoice_item` (
  `invoice_item_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `tax` decimal(15,4) NOT NULL,
  `discount` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_invoice_item`
--

INSERT INTO `vfcst_invoice_item` (`invoice_item_id`, `invoice_id`, `title`, `description`, `tax_class_id`, `quantity`, `price`, `tax`, `discount`) VALUES
(1, 1, 'Levis 513 Size 30 (L513030)', '', 1, 1, '950.0000', '66.5000', '0.0000'),
(3, 2, 'Levis 513 Size 30 (L513030)', '', 6, 5, '950.0000', '266.0000', '0.0000'),
(4, 3, 'Levis 513 Size 30 (L513030)', '', 1, 3, '950.0000', '47.0000', '10.0000'),
(5, 3, 'Levis 513 Size 32 (L513032)', '', 1, 1, '500.0000', '24.5000', '10.0000'),
(6, 4, 'LOGO', 'Logo Fentrogreen', 0, 1, '2000.0000', '0.0000', '0.0000'),
(7, 4, 'Business Card', 'Business Card\r\n1. Sunil Kumar\r\n2. Sudhir Kumar\r\n3. Dharmendra Kumar', 0, 3, '750.0000', '0.0000', '0.0000'),
(8, 4, 'Letterhead', 'Letterhead Fentrogreen', 0, 1, '450.0000', '0.0000', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_invoice_total`
--

CREATE TABLE `vfcst_invoice_total` (
  `invoice_total_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_invoice_total`
--

INSERT INTO `vfcst_invoice_total` (`invoice_total_id`, `invoice_id`, `code`, `title`, `value`, `sort_order`) VALUES
(1, 1, 'sub_total', 'Sub-Total', '950.0000', 1),
(2, 1, 'tax', 'GST', '66.5000', 3),
(3, 1, 'total', 'Total', '1016.5000', 9),
(4, 2, 'sub_total', 'Sub-Total', '4750.0000', 1),
(5, 2, 'tax', 'GST 28', '1330.0000', 3),
(6, 2, 'total', 'Total', '6080.0000', 9),
(7, 3, 'sub_total', 'Sub-Total', '3310.0000', 1),
(8, 3, 'tax', 'GST 5', '165.5000', 3),
(9, 3, 'total', 'Total', '3475.5000', 9),
(10, 4, 'sub_total', 'Sub-Total', '4700.0000', 1),
(11, 4, 'total', 'Total', '4700.0000', 9);

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_language`
--

CREATE TABLE `vfcst_language` (
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_language`
--

INSERT INTO `vfcst_language` (`language_id`, `name`, `code`, `locale`, `image`, `sort_order`, `status`) VALUES
(1, 'English', 'en-gb', 'en_US.UTF-8,en_US,en-gb,english', 'gb.png', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_recurring`
--

CREATE TABLE `vfcst_recurring` (
  `recurring_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `payment_code` varchar(255) NOT NULL,
  `payment_name` varchar(255) NOT NULL,
  `payment_description` text NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL DEFAULT '1.00000000',
  `comment` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `cycle` varchar(15) NOT NULL,
  `date_due` date NOT NULL DEFAULT '0000-00-00',
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_recurring_item`
--

CREATE TABLE `vfcst_recurring_item` (
  `recurring_item_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `tax` decimal(15,4) NOT NULL,
  `discount` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_recurring_total`
--

CREATE TABLE `vfcst_recurring_total` (
  `recurring_total_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_setting`
--

CREATE TABLE `vfcst_setting` (
  `setting_id` int(11) NOT NULL,
  `group` varchar(32) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_setting`
--

INSERT INTO `vfcst_setting` (`setting_id`, `group`, `key`, `value`, `serialized`) VALUES
(7, 'bank_transfer', 'bank_transfer_details', '{\"1\":\"Please make transfer to the following:\\r\\n\\r\\nBank: My Bank\\r\\nAccount No.: 1234-5678-90\"}', 1),
(11, 'total', 'total_status', '1', 0),
(12, 'total', 'total_sort_order', '9', 0),
(13, 'sub_total', 'sub_total_sort_order', '1', 0),
(36, 'sub_total', 'sub_total_status', '1', 0),
(37, 'cheque', 'cheque_completed_status_id', '5', 0),
(44, 'cheque', 'cheque_details', '{\"1\":\"Please send cheque to\\r\\n\\r\\n1 Test Street\\r\\nSingapore 123456\"}', 1),
(45, 'cheque', 'cheque_payable', 'My Company Ltd', 0),
(46, 'tax', 'tax_status', '1', 0),
(47, 'tax', 'tax_sort_order', '3', 0),
(435, 'config', 'config_paid_status', '[\"4\"]', 1),
(436, 'config', 'config_pending_status', '[\"1\",\"5\"]', 1),
(437, 'config', 'config_void_status', '[\"6\"]', 1),
(438, 'config', 'config_default_overdue_status', '3', 0),
(439, 'config', 'config_default_void_status', '6', 0),
(440, 'config', 'config_recurring_invoice_days', '14', 0),
(441, 'config', 'config_recurring_disable_days', '21', 0),
(442, 'config', 'config_recurring_default_status', '5', 0),
(443, 'config', 'config_mail', '{\"protocol\":\"mail\",\"parameter\":\"\",\"smtp_hostname\":\"\",\"smtp_username\":\"\",\"smtp_password\":\"\",\"smtp_port\":\"25\",\"smtp_timeout\":\"5\"}', 1),
(444, 'config', 'config_secure', '0', 0),
(445, 'config', 'config_seo_url', '0', 0),
(446, 'config', 'config_maintenance', '0', 0),
(447, 'config', 'config_compression', '0', 0),
(448, 'config', 'config_cache', 'file', 0),
(449, 'config', 'config_error_display', '0', 0),
(450, 'config', 'config_error_log', '1', 0),
(451, 'config', 'config_error_filename', 'error.log', 0),
(452, 'config', 'config_cron_user_id', '2', 0),
(453, 'config', 'config_google_analytics', '', 0),
(427, 'config', 'files', '', 0),
(428, 'config', 'config_currency', 'INR', 0),
(429, 'config', 'config_financial_year', '31/12', 0),
(430, 'config', 'config_auto_update_currency', '0', 0),
(431, 'config', 'config_invoice_prefix', 'INV-2017-00', 0),
(432, 'config', 'config_invoice_void_days', '7', 0),
(433, 'config', 'config_draft_status', '[\"2\"]', 1),
(434, 'config', 'config_overdue_status', '[\"3\"]', 1),
(407, 'credit', 'credit_sort_order', '', 0),
(57, 'cheque', 'cheque_status', '1', 0),
(426, 'config', 'config_home', '{\"1\":\"&lt;div class=&quot;header&quot;&gt;\\r\\n  &lt;div class=&quot;container&quot;&gt;\\r\\n    &lt;h1&gt;Divertido&lt;\\/h1&gt;&lt;\\/div&gt;&lt;\\/div&gt;\"}', 1),
(423, 'config', 'config_registration', '1', 0),
(424, 'config', 'config_meta_title', '{\"1\":\"Divertido\"}', 1),
(425, 'config', 'config_meta_description', '{\"1\":\"\"}', 1),
(69, 'cheque', 'cheque_sort_order', '0', 0),
(70, 'bank_transfer', 'bank_transfer_completed_status_id', '5', 0),
(71, 'bank_transfer', 'bank_transfer_status', '1', 0),
(72, 'bank_transfer', 'bank_transfer_sort_order', '0', 0),
(406, 'credit', 'credit_status', '1', 0),
(405, 'credit', 'credit_completed_status_id', '5', 0),
(422, 'config', 'config_forgotten_application', '1', 0),
(77, 'contact_form', 'contact_form_receiving_email', 'test@example.com', 0),
(78, 'contact_form', 'contact_form_description', '{\"1\":\"\"}', 1),
(79, 'contact_form', 'contact_form_status', '1', 0),
(421, 'config', 'config_forgotten_admin', '1', 0),
(420, 'config', 'config_language', 'en-gb', 0),
(418, 'config', 'config_limit_application', '10', 0),
(419, 'config', 'config_admin_language', 'en-gb', 0),
(417, 'config', 'config_limit_admin', '20', 0),
(416, 'config', 'config_icon', 'upload/divertido/logo.png', 0),
(415, 'config', 'config_logo', 'upload/divertido/logo.png', 0),
(413, 'config', 'config_fax', '', 0),
(414, 'config', 'config_theme', 'default', 0),
(412, 'config', 'config_telephone', '9123494061', 0),
(411, 'config', 'config_email', 'divertido4m@gmail.com', 0),
(410, 'config', 'config_address', '631/A8 , Hindpiri\r\nMain Road \r\nRanchi\r\n\r\nGSTIN : 20BJEPA5588B1ZS', 0),
(409, 'config', 'config_registered_name', 'Divertido', 0),
(408, 'config', 'config_name', 'Divertido Designs', 0);

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_status`
--

CREATE TABLE `vfcst_status` (
  `status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_status`
--

INSERT INTO `vfcst_status` (`status_id`, `language_id`, `name`) VALUES
(1, 1, 'Approved'),
(2, 1, 'Draft'),
(3, 1, 'Overdue'),
(4, 1, 'Paid'),
(5, 1, 'Pending'),
(6, 1, 'Void');

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_tax_class`
--

CREATE TABLE `vfcst_tax_class` (
  `tax_class_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_tax_class`
--

INSERT INTO `vfcst_tax_class` (`tax_class_id`, `name`, `description`) VALUES
(1, 'GST 5', '5% GST'),
(4, 'GST 12', '12% GST'),
(5, 'GST 18', '18% GST'),
(6, 'GST 28', '28% GST');

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_tax_rate`
--

CREATE TABLE `vfcst_tax_rate` (
  `tax_rate_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `rate` decimal(15,4) NOT NULL,
  `type` char(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_tax_rate`
--

INSERT INTO `vfcst_tax_rate` (`tax_rate_id`, `name`, `rate`, `type`) VALUES
(1, 'GST 5', '5.0000', 'P'),
(2, 'GST 12', '12.0000', 'P'),
(3, 'GST 18', '18.0000', 'P'),
(4, 'GST 28', '28.0000', 'P');

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_tax_rate_to_tax_class`
--

CREATE TABLE `vfcst_tax_rate_to_tax_class` (
  `tax_rate_to_tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_tax_rate_to_tax_class`
--

INSERT INTO `vfcst_tax_rate_to_tax_class` (`tax_rate_to_tax_class_id`, `tax_rate_id`, `tax_class_id`, `priority`) VALUES
(6, 1, 1, 1),
(7, 2, 4, 2),
(8, 3, 5, 3),
(9, 4, 6, 4);

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_transaction`
--

CREATE TABLE `vfcst_transaction` (
  `transaction_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL,
  `invoice_id` int(11) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_transaction_account`
--

CREATE TABLE `vfcst_transaction_account` (
  `transaction_account_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `debit` decimal(15,4) NOT NULL,
  `credit` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_url_alias`
--

CREATE TABLE `vfcst_url_alias` (
  `url_alias_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_url_alias`
--

INSERT INTO `vfcst_url_alias` (`url_alias_id`, `language_id`, `query`, `keyword`) VALUES
(1, 1, 'article_id=1', 'about-us'),
(2, 1, 'article_id=3', 'how-to-pay'),
(3, 1, 'article_id=2', 'history'),
(4, 1, 'blog_category_id=1', 'tech-news'),
(5, 1, 'blog_category_id=2', 'mobile'),
(6, 1, 'blog_category_id=3', '4-inch'),
(7, 1, 'blog_category_id=4', 'desktop'),
(8, 1, 'blog_category_id=5', 'company-news'),
(9, 1, 'blog_category_id=6', 'new-products'),
(10, 1, 'blog_post_id=2', 'cool-phone-20');

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_user`
--

CREATE TABLE `vfcst_user` (
  `user_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `secret` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `email` varchar(96) NOT NULL,
  `username` varchar(32) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `password` varchar(40) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `code` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_user`
--

INSERT INTO `vfcst_user` (`user_id`, `user_group_id`, `key`, `secret`, `name`, `email`, `username`, `salt`, `password`, `ip`, `code`, `status`, `date_added`, `date_modified`) VALUES
(1, 1, 'aFHSBOqWjEtALfaaM7tbA1vISX9SxxoZNyJ9ddKdbxdyIJ1jYKouUhnGu5E0GFQS', 'xgRcQwim6DTQg9VsMqXhfjjAsJmwuD1NMzLOVXU4dnaBO1teuKrBiNx63dMRtaTK', 'Logic Invoice', 'mdsbzalam@gmail.com', 'admin', '041816a54', '4caa2694d2f2e9f02fddf3463512eca080c5909d', '127.0.0.1', '', 1, '2017-09-13 22:32:12', '2017-09-13 22:32:12'),
(2, 2, 'BSYzOfA9ov7SbDKlDDGa6pvu96nXlHqfTpVi4fqUaqTMVXq2wSN6kobgYPs6KI9w', 'vzhHrm7vNbkzTObHD1FGguXKgp5WOyP6TmE6n0NHSCOCQtzqVtXQwaC6NTfSHXLm', 'System User', 'mdsbzalam@gmail.com', 'Cron', '08797da57', 'bf7599e183c2ad3a39e26d59ed809e6c7075bfae', '', '', 1, '2017-09-13 22:32:12', '2017-09-13 22:32:12'),
(4, 3, '5RLbgCK6cjkazeT7eCJOaO3LBdMSUKCB3pcZMT70G5H0FAlyWYLCyotSjVg0wPq8', 's4SgQWQ758Nwkf5hFiDxNUyXwvwvjYsDqOR34bf3WuauH4X1BRDSulfoMQf2BhA2', 'Mahtab Alam', 'mahtab.ceh@gmail.com', 'mahtab', '114bfac6e', 'da551687aaa98d8da39bbb3f197b30908d77154e', '127.0.0.1', '5b8e3a8554681d93b7419090cf4fe83d1e32c5e0', 1, '2017-09-13 23:05:35', '2017-09-13 23:05:35');

-- --------------------------------------------------------

--
-- Table structure for table `vfcst_user_group`
--

CREATE TABLE `vfcst_user_group` (
  `user_group_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `permission` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vfcst_user_group`
--

INSERT INTO `vfcst_user_group` (`user_group_id`, `name`, `permission`) VALUES
(1, 'Top Administrator', '{\"access\":{\"0\":\"accounting/account\",\"1\":\"accounting/currency\",\"2\":\"accounting/inventory\",\"3\":\"accounting/journal\",\"4\":\"accounting/tax_class\",\"5\":\"accounting/tax_rate\",\"6\":\"billing/customer\",\"7\":\"billing/invoice\",\"8\":\"billing/recurring\",\"9\":\"common/dashboard\",\"10\":\"content/article\",\"11\":\"content/blog_category\",\"12\":\"content/blog_post\",\"13\":\"content/email_template\",\"14\":\"extension/module\",\"15\":\"extension/payment\",\"16\":\"extension/total\",\"17\":\"report/chart_of_accounts\",\"18\":\"report/invoice\",\"19\":\"report/recurring\",\"20\":\"report/sci\",\"21\":\"report/sfp\",\"22\":\"system/activity\",\"23\":\"system/error\",\"24\":\"system/filemanager\",\"25\":\"system/language\",\"26\":\"system/setting\",\"27\":\"system/status\",\"28\":\"system/user\",\"29\":\"system/user_group\",\"30\":\"module/contact_form\",\"31\":\"payment/bank_transfer\",\"32\":\"payment/cheque\",\"34\":\"total/sub_total\",\"35\":\"total/tax\",\"36\":\"total/total\",\"37\":\"payment/credit\"},\"modify\":{\"0\":\"accounting/account\",\"1\":\"accounting/currency\",\"2\":\"accounting/inventory\",\"3\":\"accounting/journal\",\"4\":\"accounting/tax_class\",\"5\":\"accounting/tax_rate\",\"6\":\"billing/customer\",\"7\":\"billing/invoice\",\"8\":\"billing/recurring\",\"9\":\"common/dashboard\",\"10\":\"content/article\",\"11\":\"content/blog_category\",\"12\":\"content/blog_post\",\"13\":\"content/email_template\",\"14\":\"extension/module\",\"15\":\"extension/payment\",\"16\":\"extension/total\",\"17\":\"report/chart_of_accounts\",\"18\":\"report/invoice\",\"19\":\"report/recurring\",\"20\":\"report/sci\",\"21\":\"report/sfp\",\"22\":\"system/activity\",\"23\":\"system/error\",\"24\":\"system/filemanager\",\"25\":\"system/language\",\"26\":\"system/setting\",\"27\":\"system/status\",\"28\":\"system/user\",\"29\":\"system/user_group\",\"30\":\"module/contact_form\",\"31\":\"payment/bank_transfer\",\"32\":\"payment/cheque\",\"34\":\"total/sub_total\",\"35\":\"total/tax\",\"36\":\"total/total\",\"37\":\"payment/credit\"}}'),
(2, 'System', ''),
(3, 'Client', '{\"access\":[\"accounting\\/inventory\",\"billing\\/customer\",\"billing\\/invoice\",\"common\\/dashboard\",\"report\\/chart_of_accounts\",\"report\\/invoice\",\"system\\/setting\",\"system\\/status\",\"module\\/contact_form\",\"total\\/sub_total\",\"total\\/tax\",\"total\\/total\"],\"modify\":[\"accounting\\/inventory\",\"billing\\/invoice\"]}');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `vfcst_account`
--
ALTER TABLE `vfcst_account`
  ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `vfcst_activity`
--
ALTER TABLE `vfcst_activity`
  ADD PRIMARY KEY (`activity_id`);

--
-- Indexes for table `vfcst_article`
--
ALTER TABLE `vfcst_article`
  ADD PRIMARY KEY (`article_id`);

--
-- Indexes for table `vfcst_article_description`
--
ALTER TABLE `vfcst_article_description`
  ADD PRIMARY KEY (`article_id`,`language_id`);

--
-- Indexes for table `vfcst_blog_category`
--
ALTER TABLE `vfcst_blog_category`
  ADD PRIMARY KEY (`blog_category_id`);

--
-- Indexes for table `vfcst_blog_category_description`
--
ALTER TABLE `vfcst_blog_category_description`
  ADD PRIMARY KEY (`blog_category_description_id`);

--
-- Indexes for table `vfcst_blog_post`
--
ALTER TABLE `vfcst_blog_post`
  ADD PRIMARY KEY (`blog_post_id`);

--
-- Indexes for table `vfcst_blog_post_description`
--
ALTER TABLE `vfcst_blog_post_description`
  ADD PRIMARY KEY (`blog_post_description_id`);

--
-- Indexes for table `vfcst_blog_post_to_blog_category`
--
ALTER TABLE `vfcst_blog_post_to_blog_category`
  ADD PRIMARY KEY (`blog_post_id`,`blog_category_id`);

--
-- Indexes for table `vfcst_currency`
--
ALTER TABLE `vfcst_currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Indexes for table `vfcst_customer`
--
ALTER TABLE `vfcst_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `vfcst_customer_credit`
--
ALTER TABLE `vfcst_customer_credit`
  ADD PRIMARY KEY (`customer_credit_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `vfcst_customer_ip`
--
ALTER TABLE `vfcst_customer_ip`
  ADD PRIMARY KEY (`customer_ip_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `vfcst_email_template`
--
ALTER TABLE `vfcst_email_template`
  ADD PRIMARY KEY (`email_template_id`);

--
-- Indexes for table `vfcst_email_template_description`
--
ALTER TABLE `vfcst_email_template_description`
  ADD PRIMARY KEY (`email_template_description_id`);

--
-- Indexes for table `vfcst_email_template_type`
--
ALTER TABLE `vfcst_email_template_type`
  ADD PRIMARY KEY (`email_template_type_id`);

--
-- Indexes for table `vfcst_extension`
--
ALTER TABLE `vfcst_extension`
  ADD PRIMARY KEY (`extension`);

--
-- Indexes for table `vfcst_inventory`
--
ALTER TABLE `vfcst_inventory`
  ADD PRIMARY KEY (`inventory_id`);

--
-- Indexes for table `vfcst_invoice`
--
ALTER TABLE `vfcst_invoice`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `vfcst_invoice_history`
--
ALTER TABLE `vfcst_invoice_history`
  ADD PRIMARY KEY (`invoice_history_id`),
  ADD KEY `invoice_id` (`invoice_id`);

--
-- Indexes for table `vfcst_invoice_item`
--
ALTER TABLE `vfcst_invoice_item`
  ADD PRIMARY KEY (`invoice_item_id`),
  ADD KEY `invoice_id` (`invoice_id`);

--
-- Indexes for table `vfcst_invoice_total`
--
ALTER TABLE `vfcst_invoice_total`
  ADD PRIMARY KEY (`invoice_total_id`),
  ADD KEY `invoice_id` (`invoice_id`);

--
-- Indexes for table `vfcst_language`
--
ALTER TABLE `vfcst_language`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `vfcst_recurring`
--
ALTER TABLE `vfcst_recurring`
  ADD PRIMARY KEY (`recurring_id`);

--
-- Indexes for table `vfcst_recurring_item`
--
ALTER TABLE `vfcst_recurring_item`
  ADD PRIMARY KEY (`recurring_item_id`),
  ADD KEY `recurring_id` (`recurring_id`);

--
-- Indexes for table `vfcst_recurring_total`
--
ALTER TABLE `vfcst_recurring_total`
  ADD PRIMARY KEY (`recurring_total_id`),
  ADD KEY `recurring_id` (`recurring_id`);

--
-- Indexes for table `vfcst_setting`
--
ALTER TABLE `vfcst_setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `vfcst_status`
--
ALTER TABLE `vfcst_status`
  ADD PRIMARY KEY (`status_id`,`language_id`);

--
-- Indexes for table `vfcst_tax_class`
--
ALTER TABLE `vfcst_tax_class`
  ADD PRIMARY KEY (`tax_class_id`);

--
-- Indexes for table `vfcst_tax_rate`
--
ALTER TABLE `vfcst_tax_rate`
  ADD PRIMARY KEY (`tax_rate_id`);

--
-- Indexes for table `vfcst_tax_rate_to_tax_class`
--
ALTER TABLE `vfcst_tax_rate_to_tax_class`
  ADD PRIMARY KEY (`tax_rate_to_tax_class_id`);

--
-- Indexes for table `vfcst_transaction`
--
ALTER TABLE `vfcst_transaction`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `vfcst_transaction_account`
--
ALTER TABLE `vfcst_transaction_account`
  ADD PRIMARY KEY (`transaction_account_id`),
  ADD KEY `transaction_id` (`transaction_id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `vfcst_url_alias`
--
ALTER TABLE `vfcst_url_alias`
  ADD PRIMARY KEY (`url_alias_id`);

--
-- Indexes for table `vfcst_user`
--
ALTER TABLE `vfcst_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `vfcst_user_group`
--
ALTER TABLE `vfcst_user_group`
  ADD PRIMARY KEY (`user_group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `vfcst_activity`
--
ALTER TABLE `vfcst_activity`
  MODIFY `activity_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `vfcst_article`
--
ALTER TABLE `vfcst_article`
  MODIFY `article_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `vfcst_blog_category`
--
ALTER TABLE `vfcst_blog_category`
  MODIFY `blog_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `vfcst_blog_category_description`
--
ALTER TABLE `vfcst_blog_category_description`
  MODIFY `blog_category_description_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `vfcst_blog_post`
--
ALTER TABLE `vfcst_blog_post`
  MODIFY `blog_post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `vfcst_blog_post_description`
--
ALTER TABLE `vfcst_blog_post_description`
  MODIFY `blog_post_description_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `vfcst_currency`
--
ALTER TABLE `vfcst_currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `vfcst_customer`
--
ALTER TABLE `vfcst_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `vfcst_customer_credit`
--
ALTER TABLE `vfcst_customer_credit`
  MODIFY `customer_credit_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vfcst_customer_ip`
--
ALTER TABLE `vfcst_customer_ip`
  MODIFY `customer_ip_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vfcst_email_template`
--
ALTER TABLE `vfcst_email_template`
  MODIFY `email_template_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `vfcst_email_template_description`
--
ALTER TABLE `vfcst_email_template_description`
  MODIFY `email_template_description_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `vfcst_email_template_type`
--
ALTER TABLE `vfcst_email_template_type`
  MODIFY `email_template_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `vfcst_extension`
--
ALTER TABLE `vfcst_extension`
  MODIFY `extension` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `vfcst_inventory`
--
ALTER TABLE `vfcst_inventory`
  MODIFY `inventory_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `vfcst_invoice`
--
ALTER TABLE `vfcst_invoice`
  MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `vfcst_invoice_history`
--
ALTER TABLE `vfcst_invoice_history`
  MODIFY `invoice_history_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `vfcst_invoice_item`
--
ALTER TABLE `vfcst_invoice_item`
  MODIFY `invoice_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `vfcst_invoice_total`
--
ALTER TABLE `vfcst_invoice_total`
  MODIFY `invoice_total_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `vfcst_language`
--
ALTER TABLE `vfcst_language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vfcst_recurring`
--
ALTER TABLE `vfcst_recurring`
  MODIFY `recurring_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vfcst_recurring_item`
--
ALTER TABLE `vfcst_recurring_item`
  MODIFY `recurring_item_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vfcst_recurring_total`
--
ALTER TABLE `vfcst_recurring_total`
  MODIFY `recurring_total_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vfcst_setting`
--
ALTER TABLE `vfcst_setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=454;
--
-- AUTO_INCREMENT for table `vfcst_status`
--
ALTER TABLE `vfcst_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `vfcst_tax_class`
--
ALTER TABLE `vfcst_tax_class`
  MODIFY `tax_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `vfcst_tax_rate`
--
ALTER TABLE `vfcst_tax_rate`
  MODIFY `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `vfcst_tax_rate_to_tax_class`
--
ALTER TABLE `vfcst_tax_rate_to_tax_class`
  MODIFY `tax_rate_to_tax_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `vfcst_transaction`
--
ALTER TABLE `vfcst_transaction`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vfcst_transaction_account`
--
ALTER TABLE `vfcst_transaction_account`
  MODIFY `transaction_account_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vfcst_url_alias`
--
ALTER TABLE `vfcst_url_alias`
  MODIFY `url_alias_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `vfcst_user`
--
ALTER TABLE `vfcst_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `vfcst_user_group`
--
ALTER TABLE `vfcst_user_group`
  MODIFY `user_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
